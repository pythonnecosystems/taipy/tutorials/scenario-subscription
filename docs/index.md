# 시나리오 구독

> [여기](https://docs.taipy.io/scenario_subscription.py)에서 코드를 다운로드받을 수 있다.

## 시나리오 구독하기
작업 상태 변경 후 동작을 수행하려면 시나리오에 [함수를 가입하면](https://docs.taipy.io/en/release-3.0/manuals/core/entities/orchestrating-and-job-execution/#subscribe-to-job-execution) 된다. 상태 변경이 있을 때 이 함수가 트리거된다. 이 기능을 사용하면 Taipy GUI에 대한 로그 또는 특정 이벤트를 만들 수 있다.

```python
def callback_scenario_state(scenario, job):
    """All the scenarios are subscribed to the callback_scenario_state function. It means whenever
    a job is done, it is called.
    Depending on the job and the status, it will update the message stored in a json that is then
    displayed on the GUI.

    Args:
        scenario (Scenario): the scenario of the job changed
        job (_type_): the job that has its status changed
    """
    print(f'{job.id} to {job.status}')

    if job.status == tp.core.Status.COMPLETED:
        for data_node in job.task.output.values():
            print("Data node value:", data_node.read())
```

그런 다음 시나리오가 이 콜백에 가입할 수 있다. 예를 들어, 다음과 같은 구성의 시나리오가 있다.

![](./config.svg)

```python
scenario = tp.create_scenarios(scenario_cfg)

scenario.subscribe(scenario_cfg)

scenario.submit()
```

출력:

```
JOB_double_... to Status.PENDING
JOB_add_... to Status.BLOCKED
JOB_double_... to Status.RUNNING
JOB_double_... to Status.COMPLETED
Data node value: 42
JOB_add_... to Status.PENDING
JOB_add_... to Status.RUNNING
JOB_add_... to Status.COMPLETED
Data node value: 52
```

### GUI의 실시간 피드백
`on_submission_change` 속성은 GUI 설정에서 이 기능을 확장한다. 이것은 각 제출 상태 변경 시 특정 기능을 트리거하여 사용자 인터페이스에 대한 실시간 업데이트를 가능하게 한다. 이는 사용자에게 SUBMITTED 부터 COMPLETED 또는 CANCELLED까지 항상 알려지도록 하여 즉각적인 피드백과 상호 작용을 통해 사용자 경험을 향상시킨다.

### 함수의 파라미터

- `state (State)`: state 인스턴스
- `Submitable (Submitable)`: 제출된 엔티티, 일반적으로 시나리오이다.
- `detail (dict)`: 이 콜백의 호출에 대한 상세내용, 제출의 새로운 상태와 상태 변경을 야기하는 작업을 포함한다

### 다양한 제출 상태 처리
다음은 코드에서 이 속성을 사용하는 방법의 예이다.

```python
from taipy.gui import Gui, notify

def on_submission_status_change(state, submittable, details):
    submission_status = details.get('submission_status')

    if submission_status == 'COMPLETED':
        print(f"{submittable.name} has completed.")
        notify(state, 'success', 'Completed!')
        # Add additional actions here, like updating the GUI or logging the completion.

    elif submission_status == 'FAILED':
        print(f"{submittable.name} has failed.")
        notify(state, 'error', 'Completed!')
        # Handle failure, like sending notifications or logging the error.

    # Add more conditions for other statuses as needed.
```

### GUI에서 구현
시나리오에 위한 GUI를 만들 때 이 기능을 실시간 업데이트를 위한 시각적 요소와 연결할 수 있다. 예를 들어 다음과 같다.

```
<|{scenario}|scenario|on_submission_change=on_submission_status_change|>
```

이 시각적 요소는 제출 상태가 변경될 때마다 업데이트되어 GUI에 실시간 피드백을 제공한다.

## 전체 코드

```python
from taipy.config import Config
from taipy.core import Status
import taipy as tp
import time


# Normal function used by Taipy
def double(nb):
    return nb * 2

def add(nb):
    return nb + 10


# Configuration of Data Nodes
input_cfg = Config.configure_data_node("input", default_data=21)
intermediate_cfg = Config.configure_data_node("intermediate")
output_cfg = Config.configure_data_node("output")


# Configuration of tasks
first_task_cfg = Config.configure_task("double",
                                       double,
                                       input_cfg,
                                       intermediate_cfg)

second_task_cfg = Config.configure_task("add",
                                        add,
                                        intermediate_cfg,
                                        output_cfg)


def callback_scenario_state(scenario, job):
    """All the scenarios are subscribed to the callback_scenario_state function. It means whenever a job is done, it is called.
    Depending on the job and the status, it will update the message stored in a json that is then displayed on the GUI.

    Args:
        scenario (Scenario): the scenario of the job changed
        job (_type_): the job that has its status changed
    """
    print(scenario.name)
    if job.status == Status.COMPLETED:
        for data_node in job.task.output.values():
            print(data_node.read())

# Configuration of scenario
scenario_cfg = Config.configure_scenario(id="my_scenario",
                                         task_configs=[first_task_cfg, second_task_cfg],
                                         name="my_scenario")


if __name__=="__main__":
    tp.Core().run()
    scenario_1 = tp.create_scenario(scenario_cfg)
    scenario_1.subscribe(callback_scenario_state)

    scenario_1.submit(wait=True)


from taipy.gui import Gui, notify

def on_submission_status_change(state=None, submittable=None, details=None):
    submission_status = details.get('submission_status')

    if submission_status == 'COMPLETED':
        print(f"{submittable.name} has completed.")
        notify(state, 'success', 'Completed!')
        # Add additional actions here, like updating the GUI or logging the completion.

    elif submission_status == 'FAILED':
        print(f"{submittable.name} has failed.")
        notify(state, 'error', 'Completed!')
        # Handle failure, like sending notifications or logging the error.

    # Add more conditions for other statuses as needed.


if __name__=="__main__":
    scenario_md = """
<|{scenario_1}|scenario|on_submission_change=on_submission_status_change|>
"""
    Gui(scenario_md).run()
```